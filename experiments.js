const numbers = document.getElementsByClassName('number');
const result = document.querySelector('.result');
const operations = document.getElementsByClassName('operation');
const equal = document.getElementById('equal');

let hasOperand = true;
let firstOperand = 0;
let operation;

for (let number of numbers) {
    number.addEventListener('click', function () {
        if (result.innerText === '0') {
            result.innerText = number.innerText;
        } else {
            result.innerText += number.innerText;
        }
    });
}

const back = document.getElementById('back');

back.addEventListener('click', function () {
    if (result.innerText.length === 1) {
        result.innerText = '0';
    } else {
        result.innerText = result.innerText.slice(0, -1);
    }
});

const clear = document.getElementById('clear');

clear.addEventListener('click', function () {
    result.innerText = '0';
});

for (let op of operations) {
    op.addEventListener('click', function () {
        if (operation == null) {
            firstOperand = Number(result.innerText); 
        } else {
            firstOperand = calculateNumber(firstOperand, Number(result.innerText), operation);
        }
        operation = op;
        result.innerText = '0';
    });
}

equal.addEventListener('click', function () {
    result.innerText = String(calculateNumber(firstOperand, Number(result.innerText), operation));
});

function calculateNumber(firstArg, _secondArg, op) {
    let ret = Number(result.innerText);
    if(operation != null) {
        if (op.id === 'delete') {
            ret = firstOperand / Number(result.innerText);
        } else if (op.id === 'times') {
            ret = firstOperand * Number(result.innerText);
        }
        else if (op.id === 'minus') {
            ret = firstOperand - Number(result.innerText);
        } else {
            ret = firstOperand + Number(result.innerText);
        }
    }
    operation = null;
    return ret;
}

// Their solution
let runningTotal = 0;
let buffer = "0";
let previousOperator;

document.querySelector('.calc-buttons')
    .addEventListener("click", function(event) {
        buttonClick(event.target.innerText);
    });

const screen = document.querySelector('.screen');

function buttonClick(value) {
    if (isNaN(parseInt(value))) {
        handleSymbol(value);
    } else {
        handleNumber(value);
    }
    rerender();
}

function handleNumber(value) {
    if (buffer === "0") {
        buffer = value;
    } else {
        buffer += value;
    }
}

function handleSymbol(value) {
    switch(value) {
        case 'C':
            buffer = "0";
            runningTotal = 0;
            previousOperator = null;
            break;
        case "=":
            if (previousOperator === null) {
                return;
            }
            flushOperation(parseInt(buffer));
            previousOperator = null;
            buffer = "" + runningTotal;
            runningTotal = 0;
            break;
        case "←":
            if (buffer.length === 1) {
                buffer = "0";
            } else {
                buffer = buffer.substring(0, buffer.length - 1);
            }
            break;
        default:
            handleMath(value);
            break;

    }
}

function handleMath(value) {
    const intBuffer = parseInt(buffer);
    if (runningTotal === 0) {
        runningTotal = intBuffer;
    } else {
        flushOperation(intBuffer);
    }
}

function flushOperation (intBuffer) {
    if (previousOperator === "+") {
        runningTotal += intBuffer;
    } else if (previousOperator === "-") {
        runningTotal -= intBuffer;
    } else if (previousOperator === "*") {
        runningTotal *= intBuffer;
    } else {
        runningTotal /= intBuffer;
    }
}

function rerender() {
    screen.innerText = buffer;
}

